package by.set.testwork.di.modules;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;

import by.set.testwork.api.GoogleApiService;
import by.set.testwork.api.YandexApiService;
import by.set.testwork.utils.RxThreadFactory;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by set.
 */

@Module
public class ApiModule {

    private final String  server;
    private static OkHttpClient CLIENT;
    private static final int TIMEOUT = 60;
    private static final int WRITE_TIMEOUT = 120;
    private static final int CONNECT_TIMEOUT = 10;

    public ApiModule (String server) {
        this.server = server;
    }

    @Provides
    @Named(GoogleApiService.NAME)
    public GoogleApiService getGoogleApiService(){
        return getRetrofit().create(GoogleApiService.class);
    }

    @Provides
    @Named(YandexApiService.NAME)
    public YandexApiService getYandexApiService(){
        return getRetrofit().create(YandexApiService.class);
    }

    private Retrofit getRetrofit(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        CLIENT = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .addNetworkInterceptor(logging)
                .build();

        Gson converter = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .setLenient()
                .create();

        return new Retrofit.Builder()
                .addCallAdapterFactory(new RxThreadFactory(Schedulers.io(), AndroidSchedulers.mainThread()))
                .baseUrl(server)
                .addConverterFactory(GsonConverterFactory.create(converter))
                .client(CLIENT)
                .build();
    }

}
