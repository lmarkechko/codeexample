package by.set.testwork.di.components;

import javax.inject.Named;

import by.set.testwork.api.GoogleApiService;
import by.set.testwork.api.YandexApiService;
import by.set.testwork.di.modules.ApiModule;
import by.set.testwork.di.scope.AppScope;
import by.set.testwork.presenters.GoogleApiPresenter;
import by.set.testwork.presenters.YandexApiPresenter;
import dagger.Component;

/**
 * Created by set.
 */

@Component(modules = ApiModule.class)
@AppScope
public interface ApiComponent {
    void inject(GoogleApiPresenter subscriber);
    void inject(YandexApiPresenter subscriber);


    @Named(GoogleApiService.NAME)
    GoogleApiService getApiService();

    @Named(YandexApiService.NAME)
    YandexApiService getApiYandexService();
}
