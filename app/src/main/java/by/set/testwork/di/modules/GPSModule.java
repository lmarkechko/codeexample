package by.set.testwork.di.modules;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import by.set.testwork.R;
import by.set.testwork.models.BaseMarkerModel;
import by.set.testwork.presenters.base.GPSPresenter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by set.
 */

@Module
public class GPSModule implements GPSPresenter, LocationListener {
    private static final String TAG = "GPSTracker";
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 100;
    private static final long MIN_TIME_BW_UPDATES = TimeUnit.MINUTES.toMillis(2);
    private LocationManager mLocationManager;
    private Location mLastLocation;
    private final Context context;
    private final List<OnLocationChangeListener> listeners = new ArrayList<>();

    public GPSModule(Context context) {
        this.context=context;
    }


    @Provides
    GPSPresenter getGPSPresenter(){
        return this;
    }

    private void initLocation() {
        Location location = null;
        try {
            mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            // getting GPS status
            boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            Log.i(TAG, "isGPSEnabled=" + isGPSEnabled);

            // getting network status
            boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            Log.i(TAG, "isNetworkEnabled=" + isNetworkEnabled);

            if (!isGPSEnabled && !isNetworkEnabled) {
                Toast.makeText(context, R.string.locations_disable, Toast.LENGTH_SHORT).show();
            } else {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context, R.string.no_permission, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (isNetworkEnabled) {

                    mLocationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    if (mLocationManager != null) {
                        location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                } else if (isGPSEnabled && location==null) {
                    mLocationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    if (mLocationManager != null) {
                        location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (location != null) {
            mLastLocation = location;
            onLocationChanged(mLastLocation);
        }
    }

    public void stopUsingGPS() {
        if (mLocationManager != null) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mLocationManager.removeUpdates(this);
            mLocationManager =null;

            Log.i(TAG, "GPS stop");
        }

    }

    public void startUsingGPS() {
        initLocation();
    }

    @Override
    public void onProviderDisabled(String provider) {
        if((provider.equals(LocationManager.GPS_PROVIDER) && !mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) ||
                (provider.equals(LocationManager.NETWORK_PROVIDER) && !mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)))
            stopUsingGPS();
    }

    @Override
    public void onProviderEnabled(String provider) {
        if((provider.equals(LocationManager.GPS_PROVIDER) && !mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) ||
                (provider.equals(LocationManager.NETWORK_PROVIDER) && !mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)))
            initLocation();
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location==null)
            return;

        mLastLocation=location;

        for(OnLocationChangeListener listener : listeners)
            listener.onLocationChanged(new BaseMarkerModel(location));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void subscribeOnLocationChange(OnLocationChangeListener listener){
        listeners.add(listener);
    }

    public void unsubscribeOnLocationChange(OnLocationChangeListener listener){
        listeners.remove(listener);
    }

    @Override
    public BaseMarkerModel getCurrentLocation() {
        return mLastLocation==null ? null : new BaseMarkerModel(mLastLocation);
    }

}
