package by.set.testwork.di.modules;


import by.set.testwork.TestworkApp;
import dagger.Module;
import dagger.Provides;

/**
 * Created by set.
 */

@Module
public class AppModule {

    private final TestworkApp application;

    public AppModule(TestworkApp application) {
        this.application = application;
    }

    @Provides
    public TestworkApp getApp(){
        return application;
    }


}
