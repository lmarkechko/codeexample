package by.set.testwork.di.modules;

import android.content.Context;

import by.set.testwork.presenters.GoogleApiPresenter;
import by.set.testwork.presenters.base.DataPresenter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by set.
 */

@Module
public class DataModule {
    private final Context context;

    public DataModule(Context context) {
        this.context = context;
    }

    @Provides
    public DataPresenter getDataPresenter() {
        return new GoogleApiPresenter(context);
    }

}
