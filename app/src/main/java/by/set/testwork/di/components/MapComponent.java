package by.set.testwork.di.components;

import javax.inject.Named;

import by.set.testwork.GoogleMapFragment;
import by.set.testwork.YandexMapFragment;
import by.set.testwork.di.modules.MapModule;
import by.set.testwork.di.scope.FragmentScope;
import by.set.testwork.presenters.GoogleMapPresenter;
import by.set.testwork.presenters.YandexMapPresenter;
import by.set.testwork.presenters.base.BaseMapPresenter;
import dagger.Component;

/**
 * Created by set.
 */

@Component(modules = MapModule.class)
@FragmentScope
public interface MapComponent {
    void inject(GoogleMapFragment fragment);
    void inject(YandexMapFragment fragment);

    @Named(GoogleMapPresenter.NAME)
    BaseMapPresenter getGooglePresenter();

    @Named(YandexMapPresenter.NAME)
    BaseMapPresenter getYandexPresenter();
}
