package by.set.testwork.di.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;
import javax.inject.Singleton;

/**
 * Created by set.
 */

@Scope
@Singleton
@Retention(RetentionPolicy.RUNTIME)
public @interface AppScope {
}
