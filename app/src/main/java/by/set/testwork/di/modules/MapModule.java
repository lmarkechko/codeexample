package by.set.testwork.di.modules;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;

import javax.inject.Named;

import by.set.testwork.presenters.GoogleMapPresenter;
import by.set.testwork.presenters.YandexMapPresenter;
import by.set.testwork.presenters.base.BaseMapPresenter;
import dagger.Module;
import dagger.Provides;
import ru.yandex.yandexmapkit.MapController;

/**
 * Created by set.
 */

@Module
public class MapModule {
    private final Context context;
    private final Object map;

    public MapModule(Context context, Object map){
        this.context=context;
        this.map=map;
    }

    @Provides
    @Named(GoogleMapPresenter.NAME)
    public BaseMapPresenter getGooglePresenter(){
        return new GoogleMapPresenter(context, (GoogleMap) map);
    }

    @Provides
    @Named(YandexMapPresenter.NAME)
    public BaseMapPresenter getYandexPresenter(){
        return new YandexMapPresenter(context, (MapController) map);
    }

}
