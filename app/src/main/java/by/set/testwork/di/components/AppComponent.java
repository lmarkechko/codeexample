package by.set.testwork.di.components;

import android.content.Context;

import by.set.testwork.TestworkApp;
import by.set.testwork.di.modules.AppModule;
import by.set.testwork.di.modules.GPSModule;
import by.set.testwork.di.scope.AppScope;
import by.set.testwork.presenters.base.GPSPresenter;
import dagger.Component;

/**
 * Created by set.
 */

@Component(modules = { AppModule.class, GPSModule.class})
@AppScope
public interface AppComponent {
    void inject(Context app);

    TestworkApp getApp();

    GPSPresenter getGPSPresenter();

}
