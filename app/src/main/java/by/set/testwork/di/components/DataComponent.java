package by.set.testwork.di.components;

import by.set.testwork.BaseDataActivity;
import by.set.testwork.di.modules.DataModule;
import by.set.testwork.di.scope.ActivityScope;
import by.set.testwork.presenters.base.DataPresenter;
import dagger.Component;

/**
 * Created by set.
 */

@Component(modules = DataModule.class)
@ActivityScope
public interface DataComponent {
    void inject(BaseDataActivity activity);

    DataPresenter getDataPresenter();
}
