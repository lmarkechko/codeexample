package by.set.testwork.adapters;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import by.set.testwork.R;
import by.set.testwork.models.BaseMarkerModel;
import by.set.testwork.models.BasePlaceModel;
import by.set.testwork.utils.CompareHelper;
import by.set.testwork.utils.Utils;

/**
 * Created by set
 */

public class PlacesListAdapter<T extends BasePlaceModel> extends BaseRecyclerAdapter<T> {

    private BaseMarkerModel point;

    public PlacesListAdapter(@Nullable BaseMarkerModel point) {
        super(new ArrayList<>());
        this.point=point;
    }

    public PlacesListAdapter(List<T> list, @Nullable BaseMarkerModel point) {
        super(list);
        setPoint(point);
    }

    public void setPoint(BaseMarkerModel point){
        this.point=point;
        Comparator<BasePlaceModel> comparator = (vm1, vm2) -> CompareHelper.nullSafeAscendingCompare(Utils.getDistance(point, vm1.getPoint()), Utils.getDistance(point, vm2.getPoint()));
        Collections.sort(objects, comparator);

        notifyDataSetChanged();
    }

    @Override
    public void setData(List<T> objects){
        if(objects==null || objects.isEmpty()) {
            this.objects.clear();
            notifyDataSetChanged();
            return;
        }

        this.objects = objects;
        Comparator<BasePlaceModel> comparator = (vm1, vm2) -> CompareHelper.nullSafeAscendingCompare(Utils.getDistance(point, vm1.getPoint()), Utils.getDistance(point, vm2.getPoint()));
        Collections.sort(objects, comparator);

        notifyDataSetChanged();
    }

    @Override
    public PlaceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_place, parent, false);
        return new PlaceHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((PlaceHolder) holder).bind(objects.get(position), point);
        addClickListeners(holder, position);
    }


    public static class PlaceHolder extends RecyclerView.ViewHolder {

        private final TextView name;
        private final ImageView icon;
        private final TextView adres;
        private final TextView distance;

        public PlaceHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            adres = (TextView) itemView.findViewById(R.id.adres);
            icon = (ImageView) itemView.findViewById(R.id.icon);
            distance = (TextView) itemView.findViewById(R.id.distance);
        }

        public void bind(BasePlaceModel item, BaseMarkerModel point){
            name.setText(item.getName());
            adres.setText(item.getAdres());
            distance.setText(distance.getResources().getString(R.string.distance, Utils.getDistance(point, item.getPoint())));

            Glide.with(name.getContext())
                    .load(item.getIcon())
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .centerCrop()
                    .into(icon);
        }
    }

}
