package by.set.testwork.adapters;

import android.support.v7.widget.RecyclerView;

import java.util.List;

import by.set.testwork.models.BasePlaceModel;

/**
 * Created by set
 */
public abstract class BaseRecyclerAdapter<T extends BasePlaceModel> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected List<T> objects;

    private OnItemClickListener onItemClickListener;


    public BaseRecyclerAdapter(List<T> objects) {
        if(objects==null)
            return;

        this.objects = objects;
    }

    @Override
    public int getItemCount() {
        if(objects==null)
            return 0;
        else
            return objects.size();
    }

    public void setData(List<T> objects){
        if(objects==null || objects.isEmpty()) {
            this.objects.clear();
            notifyDataSetChanged();
            return;
        }

        this.objects = objects;
        notifyDataSetChanged();
    }

    public int getItemPosition(T object){
        for(int i=0; i< objects.size(); i++) {
            if (object.getId().equals(objects.get(i).getId()))
                return i;
        }

        return -1;
    }

    public T getItemById(String id){
        for(T object : objects) {
            if (object.getId().equals(id))
                return object;
        }

        return null;
    }

    protected void addClickListeners(RecyclerView.ViewHolder holder, int position){
        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(v -> {
            T item;
            try {
                item = objects.get(Integer.valueOf(v.getTag().toString()));
            } catch (Exception e) {
                return;
            }

            if (onItemClickListener != null)
                onItemClickListener.onItemClick(item);
        });
    }


    public void swapData(){
        if(objects==null)
            return;

        objects.clear();
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.onItemClickListener =listener;
    }

    public interface OnItemClickListener<T> {
        void onItemClick(T item);
    }
}
