package by.set.testwork;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.ChangeTransform;
import android.transition.TransitionSet;
import android.util.Log;
import android.view.View;

/**
 * Created by set.
 */

public class BaseFragmentActivity extends AppCompatActivity {


    public void changeContent(Fragment fragment, @IdRes int container, boolean inBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        String newTag = ((Object) fragment).getClass().getName() + ":"
                + (fragmentManager.getFragments() == null ? fragmentManager.getBackStackEntryCount() : fragmentManager.getFragments().size());
        FragmentTransaction transaction = fragmentManager.beginTransaction().replace(container, fragment, newTag);

        transaction.commitAllowingStateLoss();
        transaction.setTransition(FragmentTransaction.TRANSIT_NONE);
        if (inBackStack) {
            transaction.addToBackStack(null);
        }
        try {
            transaction.commit();
        } catch (IllegalStateException e) {
            Log.e("change_content", e.getLocalizedMessage());
        }
    }

    /**
     * @param view must have TransitionName.
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void changeContentWithEffects(Fragment fragment, View view, @IdRes int container, boolean inBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        TransitionSet transitionSet = new TransitionSet();
        transitionSet.addTransition(new ChangeImageTransform());
        transitionSet.addTransition(new ChangeBounds());
        transitionSet.addTransition(new ChangeTransform());
        transitionSet.setDuration(300);

        fragment.setSharedElementEnterTransition(transitionSet);
        fragment.setSharedElementReturnTransition(transitionSet);

        String newTag = ((Object) fragment).getClass().getName() + ":"
                + (fragmentManager.getFragments() == null ? fragmentManager.getBackStackEntryCount() : fragmentManager.getFragments().size());

        FragmentTransaction transaction = fragmentManager.beginTransaction().replace(container, fragment, newTag);

        transaction.commitAllowingStateLoss();
        transaction.addSharedElement(view, view.getTransitionName());

        if (inBackStack) {
            transaction.addToBackStack(null);
        }

        try {
            transaction.commit();
        } catch (IllegalStateException e) {
            Log.e("change_content", e.getLocalizedMessage());
        }
    }
}
