package by.set.testwork.api;


import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by set
 */
public interface GoogleApiService {
    String NAME = "GoogleApiService";

    @GET("/maps/api/place/nearbysearch/json")
    Observable<GoogleResponse> search(@Query("location") String loc,
                                      @Query("radius") long radius,
                                      @Query("types") String type,
                                      @Query("sensor") boolean sensor,
                                      @Query("language") String language,
                                      @Query("key") String key);
}
