package by.set.testwork.api;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import by.set.testwork.models.google.ResultGoogle;

/**
 * Created by set
 */
public class YandexResponse {

    private String error_message = "";
    private List<String> html_attributions = new ArrayList<>();
    private List<ResultGoogle> results = new ArrayList<>();
    private String status = "";

    public YandexResponse(){}

    public YandexResponse(String error){
        status="ERROR";
        error_message = error;
    }

    public List<ResultGoogle> getResult() {
        return results;
    }

    public List<String> getAttributions() {
        return html_attributions;
    }

    public String getStatus() {
        return status;
    }

    public String getErrorMessage() {
        StringBuilder str = new StringBuilder();

        if(status!=null && !status.isEmpty())
            str.append(status);

        if(error_message!=null && !error_message.isEmpty()) {
            if(str.length()>0)
                str.append("\n");
            str.append(error_message);
        }

        return str.toString();
    }

    public static YandexResponse parseResponse(String message){
        try {
            return (new Gson()).fromJson(message, YandexResponse.class);
        } catch (Exception e){
            return new YandexResponse(message);
        }
    }

    public static YandexResponse parseResponse(Throwable error){
        return new YandexResponse(error.getLocalizedMessage()!=null ? error.getLocalizedMessage() : error.getMessage());
    }
}
