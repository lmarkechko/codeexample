package by.set.testwork.api;


import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by set
 */
public interface YandexApiService {
    String NAME = "YandexApiService";

    @GET("/v1/")
    Observable<YandexResponse> search(@Query("ll") String loc,
                                      @Query("spn") String radius,
                                      @Query("type") String type,
                                      @Query("lang") String language,
                                      @Query("apikey") String key);
}
