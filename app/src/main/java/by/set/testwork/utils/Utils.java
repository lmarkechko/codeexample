package by.set.testwork.utils;

import android.content.res.Resources;
import android.location.Location;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;

import com.google.android.gms.maps.model.LatLng;

import by.set.testwork.models.BaseMarkerModel;
import ru.yandex.yandexmapkit.utils.GeoPoint;

/**
 * Created by set
 */

public class Utils {

    public static int convertDpToPixel(float dp){
        Resources resources = Resources.getSystem();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return Math.round(px);
    }

    public static long getDistance(@Nullable BaseMarkerModel point1, @Nullable BaseMarkerModel point2){
        if(point1==null || point2==null)
            return 0;

        Location loc1 = new Location("");
        loc1.setLatitude(point1.getLatitude());
        loc1.setLongitude(point1.getLongitude());

        Location loc2 = new Location("");
        loc2.setLatitude(point2.getLatitude());
        loc2.setLongitude(point2.getLongitude());

        return Math.round(loc1.distanceTo(loc2));
    }

    public static long getDistance(@Nullable LatLng point1, @Nullable LatLng point2){
        if(point1==null || point2==null)
            return 0;

        Location loc1 = new Location("");
        loc1.setLatitude(point1.latitude);
        loc1.setLongitude(point1.longitude);

        Location loc2 = new Location("");
        loc2.setLatitude(point2.latitude);
        loc2.setLongitude(point2.longitude);

        return Math.round(loc1.distanceTo(loc2));
    }

    public static long getDistance(@Nullable GeoPoint point1, @Nullable GeoPoint point2){
        if(point1==null || point2==null)
            return 0;

        Location loc1 = new Location("");
        loc1.setLatitude(point1.getLat());
        loc1.setLongitude(point1.getLon());

        Location loc2 = new Location("");
        loc2.setLatitude(point2.getLat());
        loc2.setLongitude(point2.getLon());

        return Math.round(loc1.distanceTo(loc2));
    }

}
