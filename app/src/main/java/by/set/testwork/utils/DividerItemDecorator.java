package by.set.testwork.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by set
 */

public class DividerItemDecorator extends RecyclerView.ItemDecoration {
    private static final int[] ATTRS = new int[]{android.R.attr.listDivider};
    private final Drawable divider;
    private int leftMargin=0;
    private int rightMargin=0;

    public DividerItemDecorator(Context context) {
        final TypedArray styledAttributes = context.obtainStyledAttributes(ATTRS);
        divider = styledAttributes.getDrawable(0);
        styledAttributes.recycle();
    }

    public DividerItemDecorator(Drawable divider) {
        this.divider=divider;
    }

    public DividerItemDecorator(Context context, int resId) {
        divider = ContextCompat.getDrawable(context, resId);
    }

    public DividerItemDecorator(Context context, int resId, int leftMargin, int rightMargin) {
        divider = ContextCompat.getDrawable(context, resId);
        this.leftMargin=Utils.convertDpToPixel(leftMargin);
        this.rightMargin=Utils.convertDpToPixel(rightMargin);
    }

    public void setLeftMarginDP(int leftMargin){
        this.leftMargin=Utils.convertDpToPixel(leftMargin);
    }

    public void setRightMarginDP(int rightMargin){
        this.rightMargin=Utils.convertDpToPixel(rightMargin);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = leftMargin+parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight()-rightMargin;

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount-1; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + divider.getIntrinsicHeight();

            divider.setBounds(left, top, right, bottom);
            divider.draw(c);
        }
    }
}