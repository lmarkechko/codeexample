package by.set.testwork.utils;

/**
 * Created by set.
 */
public class CompareHelper {

    private static Long nullSafe(Long d) {
        return d != null ? d : Long.MIN_VALUE;
    }

    public static int nullSafeAscendingCompare(Long d1, Long d2) {
        return nullSafe(d1).compareTo(nullSafe(d2));
    }
}
