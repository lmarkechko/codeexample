package by.set.testwork;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import by.set.testwork.di.components.DaggerMapComponent;
import by.set.testwork.di.modules.MapModule;
import by.set.testwork.models.BaseMarkerModel;
import by.set.testwork.models.BasePlaceModel;
import by.set.testwork.models.google.MarkerGoogleModel;
import by.set.testwork.models.google.ResultGoogle;
import by.set.testwork.presenters.GoogleMapPresenter;
import by.set.testwork.presenters.base.BaseMapPresenter;
import by.set.testwork.utils.Utils;
import rx.functions.Action1;

import static by.set.testwork.R.id.map;


/**
 * Created by set.
 */

public class GoogleMapFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnCameraIdleListener, Action1<List<ResultGoogle>> {

    @Inject
    @Named(GoogleMapPresenter.NAME)
    protected BaseMapPresenter mapPresenter;

    private MapView mMapView;
    private GoogleMap mMap;
    private LatLng oldCenter;
    private long oldRadius;

    public static GoogleMapFragment getInstance() {
        return new GoogleMapFragment();
    }

    private final BroadcastReceiver dataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(mMap==null || !intent.getExtras().containsKey(MainActivity.EXTRA_DATA))
                return;

            ArrayList<BasePlaceModel> places = intent.getParcelableArrayListExtra(MainActivity.EXTRA_DATA);
            mapPresenter.makeMarkers(places);

            if(intent.getAction().equals(MainActivity.ACTION_TO_DATA)){
                CameraUpdate cameraUpdate;
                cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(places.get(0).getPoint().getLatitude(),
                        places.get(0).getPoint().getLongitude()), 16);
                mMap.moveCamera(cameraUpdate);
            }

        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_map_google, container, false);
        mMapView = (MapView) root.findViewById(map);

        IntentFilter filter = new IntentFilter(MainActivity.ACTION_SHOW_DATA);
        filter.addAction(MainActivity.ACTION_TO_DATA);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(dataReceiver, filter);
        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(dataReceiver);
        if(mMapView!=null)
            mMapView.onDestroy();
    }

    @Override
    public void onLowMemory(){
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onCreate(null);
        mMapView.onResume();
        initMap();
    }

    private void initMap() {
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Have Google Map but then error", e);
        } finally {
            mMapView.getMapAsync(this::onMapReady);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        DaggerMapComponent.builder().mapModule(new MapModule(getContext(), googleMap)).build().inject(this);

        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setTrafficEnabled(false);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setIndoorLevelPickerEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setOnCameraIdleListener(this);

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
        } else {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);

            BaseMarkerModel loc = ((TestworkApp)getActivity().getApplication()).getGpsPresenter().getCurrentLocation();

            if(loc!=null) {
                CameraUpdate cameraUpdate;
                cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(loc.getLatitude(), loc.getLongitude()), 16);
                mMap.moveCamera(cameraUpdate);
            }
        }

        oldCenter=mMap.getCameraPosition().target;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onCameraIdle() {
        LatLng newMapCenter = mMap.getCameraPosition().target;
        long newRadius = Utils.getDistance(mMap.getProjection().getVisibleRegion().farLeft, oldCenter);

        if(newRadius != oldRadius || Utils.getDistance(newMapCenter, oldCenter) > oldRadius/2){
            getDataPresenter().getCompanyServices(new MarkerGoogleModel(newMapCenter), String.valueOf(newRadius), this);
            oldCenter=newMapCenter;
            oldRadius = newRadius;
        }
    }

    @Override
    public void call(List<ResultGoogle> places) {
        if(mapPresenter!=null)
            mapPresenter.makeMarkers(new ArrayList<>(places));
    }
}
