package by.set.testwork.presenters.base;

import by.set.testwork.models.BaseMarkerModel;

/**
 * Created by set.
 */

public interface GPSPresenter {
    void stopUsingGPS();

    void startUsingGPS();

    void subscribeOnLocationChange(OnLocationChangeListener listener);

    void unsubscribeOnLocationChange(OnLocationChangeListener listener);

    interface OnLocationChangeListener {
        void onLocationChanged(BaseMarkerModel location);
    }

    BaseMarkerModel getCurrentLocation();
}
