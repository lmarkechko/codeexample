package by.set.testwork.presenters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import java.util.ArrayList;

import by.set.testwork.R;
import by.set.testwork.models.BasePlaceModel;
import by.set.testwork.models.google.PlaceGoogleModel;
import by.set.testwork.presenters.base.BaseMapPresenter;

/**
 * Created by set.
 */

public class GoogleMapPresenter implements BaseMapPresenter {
    public static final String NAME = "GoogleMapPresenter";

    private final GoogleMap mMap;
    private final ClusterManager<PlaceGoogleModel> mClusterManager;
    private final DefaultClusterRenderer mClusterRenderer;
    private PlaceGoogleModel clickedItem;

    public GoogleMapPresenter(@NonNull Context context, @NonNull GoogleMap mMap) {
        this.mMap = mMap;

        mClusterManager = new ClusterManager(context, mMap);
        mClusterRenderer = (DefaultClusterRenderer) mClusterManager.getRenderer();
        this.mMap.setOnCameraIdleListener(mClusterManager);
        this.mMap.setOnMarkerClickListener(mClusterManager);
        this.mMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(new ClusterInfoWindow(LayoutInflater.from(context)));
        mClusterManager.setOnClusterItemClickListener(item -> { clickedItem=item; return false; });
    }

    public void clear(){
        mClusterManager.clearItems();
        mMap.clear();
    }

    public void makeMarker(BasePlaceModel markerObject, boolean needCameraUpdate){
        PlaceGoogleModel object =  new PlaceGoogleModel(markerObject);

        Marker marker = mClusterRenderer.getMarker(object);
        if(marker==null) {
            mClusterManager.addItem(object);
        }

        if(needCameraUpdate)
            mMap.moveCamera(CameraUpdateFactory.newLatLng(object.getPosition()));
    }

    synchronized public void makeMarkers(@NonNull ArrayList<BasePlaceModel> markers){
        if(markers==null || markers.isEmpty())
            return;

        for(BasePlaceModel object : markers)
            makeMarker(object, false);

        mClusterManager.cluster();
    }

    private class ClusterInfoWindow implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;
        private final TextView title;

        public ClusterInfoWindow(LayoutInflater inflater) {
            myContentsView = inflater.inflate(R.layout.item_tooltip, null);
            title = ((TextView) myContentsView.findViewById(R.id.name));
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            if(clickedItem!=null) {
                title.setText(clickedItem.getName());
            }

            return myContentsView;
        }
    }

}
