package by.set.testwork.presenters.base;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import by.set.testwork.models.BasePlaceModel;

/**
 * Created by set.
 */

public interface BaseMapPresenter {
    void clear();

    void makeMarker(BasePlaceModel marker, boolean needCameraUpdate);

    void makeMarkers(@NonNull ArrayList<BasePlaceModel> markers);
}
