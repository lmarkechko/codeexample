package by.set.testwork.presenters;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import by.set.testwork.R;
import by.set.testwork.models.BasePlaceModel;
import by.set.testwork.models.yandex.PlaceYandexModel;
import by.set.testwork.presenters.base.BaseMapPresenter;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.balloon.BalloonItem;

/**
 * Created by set.
 */

public class YandexMapPresenter implements BaseMapPresenter {
    public static final String NAME = "YandexMapPresenter";

    private final MapController mapController;
    private final OverlayManager overlayManager;
    private final Context context;
    private Overlay overlay;
    private Map<BasePlaceModel, BalloonItem> places = new HashMap<>();

    public YandexMapPresenter(@NonNull Context context, @NonNull MapController mapController) {
        this.mapController = mapController;
        overlayManager = mapController.getOverlayManager();
        this.context=context;
        overlay = new Overlay(mapController);
        overlayManager.addOverlay(overlay);
    }

    public void clear(){
        overlay.clearOverlayItems();
        overlayManager.removeOverlay(overlay);
        overlay = new Overlay(mapController);
        overlayManager.addOverlay(overlay);
        places.clear();
    }

    public void makeMarker(BasePlaceModel markerObject, boolean needCameraUpdate){
        PlaceYandexModel object =  new PlaceYandexModel(markerObject);

        if(!places.containsKey(object)) {
            BalloonItem balloon = new BalloonItem(context, object.getPosition());
            balloon.setText(object.getName());
            balloon.setDrawable(context.getResources().getDrawable(R.drawable.ic_yandex_balloon));

            overlay.addOverlayItem(balloon);
            places.put(object, balloon);
        }

        if(needCameraUpdate)
            mapController.setPositionAnimationTo(object.getPosition(), 16);
    }

    synchronized public void makeMarkers(@NonNull ArrayList<BasePlaceModel> markers){
        for(BasePlaceModel marker : markers)
            makeMarker(marker, false);
    }

}
