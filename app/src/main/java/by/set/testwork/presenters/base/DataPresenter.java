package by.set.testwork.presenters.base;

import by.set.testwork.models.BaseMarkerModel;
import rx.functions.Action1;

/**
 * Created by set.
 */

public interface DataPresenter {

    void getCompanyServices(BaseMarkerModel location, String radius, Action1 callback);

    void unsubscribe();
}
