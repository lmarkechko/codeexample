package by.set.testwork.presenters;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Named;

import by.set.testwork.R;
import by.set.testwork.api.GoogleApiService;
import by.set.testwork.api.GoogleResponse;
import by.set.testwork.di.components.DaggerApiComponent;
import by.set.testwork.di.modules.ApiModule;
import by.set.testwork.models.BaseMarkerModel;
import by.set.testwork.presenters.base.DataPresenter;
import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by set
 */

public class GoogleApiPresenter implements DataPresenter {

    @Inject
    @Named(GoogleApiService.NAME)
    protected GoogleApiService api;

    private final Handler handler;
    private final Action1<Throwable> mOnError;
    private final Action1<GoogleResponse> mOnErrorGoogle;
    private final Context context;
    private final CompositeSubscription subscription = new CompositeSubscription();

    public GoogleApiPresenter(Context context) {
        this.context=context;
        this.handler = new Handler(context.getMainLooper());

        DaggerApiComponent.builder().apiModule(new ApiModule(context.getString(R.string.google_api_server))).build().inject(this);

        mOnError  = throwable -> {
            GoogleResponse error = parseThrowable(throwable);
            Log.e("NetworkRequest", error.getErrorMessage());
            handler.post(() -> Toast.makeText(context, error.getErrorMessage(), Toast.LENGTH_SHORT).show());
        };

        mOnErrorGoogle = response -> {
            Log.e("NetworkRequest", response.getErrorMessage());
            handler.post(() -> Toast.makeText(context, response.getErrorMessage(), Toast.LENGTH_SHORT).show());
        };
    }

    private GoogleResponse parseThrowable(Throwable throwable){
        if((throwable instanceof HttpException) && (((HttpException)throwable).response()).errorBody()!=null) {
            ResponseBody body = (((HttpException) throwable).response()).errorBody();
            String msg;
            try {
                msg = body.source().readUtf8();
            } catch (IOException e) {
                msg=(((HttpException) throwable).response()).message();
            }
            if(msg==null || msg.isEmpty())
                msg=(((HttpException) throwable).response()).message();

            return GoogleResponse.parseResponse(msg);

        } else {
            return GoogleResponse.parseResponse(throwable);
        }
    }

    public void getCompanyServices(BaseMarkerModel location, String radius, Action1 callback) {
        subscription.add(api.search(location.toQueryString(), Integer.valueOf(radius), "cafe", true, Locale.getDefault().toString(),
                context.getString(R.string.google_places_key))
                .retry(3)
                .cache()
                .subscribe(result -> {
                            if(!result.getStatus().equals("OK")) {
                                mOnErrorGoogle.call(result);
                            } else {
                                callback.call(result.getResult());
                            }
                        },
                        mOnError::call));
    }

    @Override
    public void unsubscribe() {
        subscription.unsubscribe();
    }

}
