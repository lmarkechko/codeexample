package by.set.testwork.presenters;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Named;

import by.set.testwork.R;
import by.set.testwork.api.YandexApiService;
import by.set.testwork.api.YandexResponse;
import by.set.testwork.di.components.DaggerApiComponent;
import by.set.testwork.di.modules.ApiModule;
import by.set.testwork.models.BaseMarkerModel;
import by.set.testwork.presenters.base.DataPresenter;
import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by set
 */

public class YandexApiPresenter implements DataPresenter {

    @Inject
    @Named(YandexApiService.NAME)
    protected YandexApiService api;

    private final Handler handler;
    private final Action1<Throwable> mOnError;
    private final Action1<YandexResponse> mOnErrorYandex;
    private final Context context;
    private final CompositeSubscription subscription = new CompositeSubscription();

    public YandexApiPresenter(Context context) {
        this.context=context;
        this.handler = new Handler(context.getMainLooper());

        DaggerApiComponent.builder().apiModule(new ApiModule(context.getString(R.string.yandex_api_server))).build().inject(this);

        mOnError  = throwable -> {
            YandexResponse error = parseThrowable(throwable);
            Log.e("NetworkRequest", error.getErrorMessage());
            handler.post(() -> Toast.makeText(context, error.getErrorMessage(), Toast.LENGTH_SHORT).show());
        };

        mOnErrorYandex = response -> {
            Log.e("NetworkRequest", response.getErrorMessage());
            handler.post(() -> Toast.makeText(context, response.getErrorMessage(), Toast.LENGTH_SHORT).show());
        };
    }

    private YandexResponse parseThrowable(Throwable throwable){
        if((throwable instanceof HttpException) && (((HttpException)throwable).response()).errorBody()!=null) {
            ResponseBody body = (((HttpException) throwable).response()).errorBody();
            String msg;
            try {
                msg = body.source().readUtf8();
            } catch (IOException e) {
                msg=(((HttpException) throwable).response()).message();
            }
            if(msg==null || msg.isEmpty())
                msg=(((HttpException) throwable).response()).message();

            return YandexResponse.parseResponse(msg);

        } else {
            return YandexResponse.parseResponse(throwable);
        }
    }

    public void getCompanyServices(BaseMarkerModel location, String radius, Action1 callback) {
        subscription.add(api.search(location.toQueryYandexString(), radius, "biz", Locale.getDefault().toString(),
                context.getString(R.string.google_places_key))
                .retry(3)
                .cache()
                .subscribe(result -> {
                            if(!result.getStatus().equals("OK")) {
                                mOnErrorYandex.call(result);
                            } else {
                                callback.call(result.getResult());
                            }
                        },
                        mOnError::call));
    }

    @Override
    public void unsubscribe() {
        subscription.unsubscribe();
    }

}
