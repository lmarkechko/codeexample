package by.set.testwork;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import by.set.testwork.di.components.DaggerMapComponent;
import by.set.testwork.di.modules.MapModule;
import by.set.testwork.models.BaseMarkerModel;
import by.set.testwork.models.BasePlaceModel;
import by.set.testwork.models.google.ResultGoogle;
import by.set.testwork.models.yandex.MarkerYandexModel;
import by.set.testwork.presenters.YandexMapPresenter;
import by.set.testwork.presenters.base.BaseMapPresenter;
import by.set.testwork.utils.Utils;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.map.MapEvent;
import ru.yandex.yandexmapkit.map.OnMapListener;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import rx.functions.Action1;

import static by.set.testwork.R.id.map;
import static ru.yandex.yandexmapkit.map.MapEvent.MSG_SCALE_END;
import static ru.yandex.yandexmapkit.map.MapEvent.MSG_SCROLL_END;
import static ru.yandex.yandexmapkit.map.MapEvent.MSG_ZOOM_END;


/**
 * Created by set.
 */

public class YandexMapFragment extends BaseFragment implements Action1<List<ResultGoogle>>, OnMapListener {

    @Inject
    @Named(YandexMapPresenter.NAME)
    protected BaseMapPresenter mapPresenter;

    private MapView mMapView;
    private MapController mMapController;
    private OverlayManager mOverlayManager;
    private GeoPoint oldCenter;
    private long oldRadius;

    public static YandexMapFragment getInstance() {
        return new YandexMapFragment();
    }

    private final BroadcastReceiver dataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(mMapController==null || !intent.getExtras().containsKey(MainActivity.EXTRA_DATA))
                return;

            ArrayList<BasePlaceModel> places = intent.getParcelableArrayListExtra(MainActivity.EXTRA_DATA);
            mapPresenter.makeMarkers(places);

            if(intent.getAction().equals(MainActivity.ACTION_TO_DATA)){
                mMapController.setPositionAnimationTo(new GeoPoint(places.get(0).getPoint().getLatitude(),
                        places.get(0).getPoint().getLongitude()), 20);
            }

        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_map_yandex, container, false);
        mMapView = (MapView) root.findViewById(map);

        IntentFilter filter = new IntentFilter(MainActivity.ACTION_SHOW_DATA);
        filter.addAction(MainActivity.ACTION_TO_DATA);
        initMap();

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(dataReceiver, filter);

        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(dataReceiver);
    }

    private void initMap() {
        mMapController = mMapView.getMapController();
        mOverlayManager = mMapController.getOverlayManager();
        mMapController.addMapListener(this);

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            mOverlayManager.getMyLocation().setEnabled(false);
        } else {
            mOverlayManager.getMyLocation().setEnabled(true);
            mMapView.post(() -> {
                BaseMarkerModel loc = ((TestworkApp) getContext().getApplicationContext()).getGpsPresenter().getCurrentLocation();
                if(loc!=null)
                    mMapController.setPositionAnimationTo(new GeoPoint(loc.getLatitude(),loc.getLongitude()), 20);

                oldCenter = mMapController.getMapCenter();
            });
        }

        DaggerMapComponent.builder().mapModule(new MapModule(getContext(), mMapController)).build().inject(this);

    }

    @Override
    public void call(List<ResultGoogle> places) {
        if(mapPresenter!=null)
            mapPresenter.makeMarkers(new ArrayList<>(places));
    }

    @Override
    public void onMapActionEvent(MapEvent mapEvent) {
        boolean needFind = false;
        switch (mapEvent.getMsg()){
            case MSG_SCROLL_END:
            case MSG_ZOOM_END:
            case MSG_SCALE_END:
                needFind=true;

        }
        if(!needFind)
            return;

        GeoPoint newMapCenter = mMapController.getMapCenter();
        long newRadius = Utils.getDistance(newMapCenter, oldCenter);

        if(newRadius != oldRadius || Utils.getDistance(newMapCenter, oldCenter) > oldRadius/2){
            getDataPresenter().getCompanyServices(new MarkerYandexModel(newMapCenter), String.valueOf(newRadius), this);
            oldCenter=newMapCenter;
            oldRadius = newRadius;
        }
    }
}
