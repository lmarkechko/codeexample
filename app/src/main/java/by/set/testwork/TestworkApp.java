package by.set.testwork;

import android.app.Application;

import by.set.testwork.di.components.AppComponent;
import by.set.testwork.di.components.DaggerAppComponent;
import by.set.testwork.di.modules.AppModule;
import by.set.testwork.di.modules.GPSModule;
import by.set.testwork.presenters.base.GPSPresenter;

/**
 * Created by set.
 */

public class TestworkApp extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).gPSModule(new GPSModule(this)).build();
    }

    public GPSPresenter getGpsPresenter(){
        return appComponent.getGPSPresenter();
    }
}
