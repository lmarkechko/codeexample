package by.set.testwork;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import by.set.testwork.adapters.BaseRecyclerAdapter;
import by.set.testwork.adapters.PlacesListAdapter;
import by.set.testwork.models.BaseMarkerModel;
import by.set.testwork.models.BasePlaceModel;
import by.set.testwork.presenters.base.GPSPresenter;
import by.set.testwork.utils.DividerItemDecorator;
import rx.functions.Action1;

/**
 * Created by set.
 */
public class MainActivity extends BaseDataActivity implements GPSPresenter.OnLocationChangeListener, Action1<List<BasePlaceModel>>,
        BaseRecyclerAdapter.OnItemClickListener<BasePlaceModel> {

    public static final String ACTION_SHOW_DATA="show_data";
    public static final String ACTION_TO_DATA="to_data";
    public static final String EXTRA_DATA="data";

    private RecyclerView list;
    private PlacesListAdapter places;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        list= (RecyclerView) findViewById(R.id.items);
        drawer = ((DrawerLayout) findViewById(R.id.drawer_layout));
        findViewById(R.id.menu_btn).setOnClickListener(v -> drawer.openDrawer(Gravity.LEFT));


        list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        list.addItemDecoration(new DividerItemDecorator(this, R.drawable.item_divider, 20, 0));

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        } else {
            changeContent(GoogleMapFragment.getInstance(), R.id.content, false);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(((TestworkApp) getApplication()).getGpsPresenter()!=null) {
            ((TestworkApp) getApplication()).getGpsPresenter().subscribeOnLocationChange(this);
            ((TestworkApp) getApplication()).getGpsPresenter().startUsingGPS();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(((TestworkApp) getApplication()).getGpsPresenter()!=null) {
            ((TestworkApp) getApplication()).getGpsPresenter().unsubscribeOnLocationChange(this);
            ((TestworkApp) getApplication()).getGpsPresenter().stopUsingGPS();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if(requestCode!=1)
            return;

        for(int result : grantResults)
            if(result!=PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, R.string.no_permission, Toast.LENGTH_SHORT).show();
                findViewById(R.id.menu_btn).setVisibility(View.GONE);
                return;
            }

        if(((TestworkApp) getApplication()).getGpsPresenter()!=null)
            ((TestworkApp) getApplication()).getGpsPresenter().startUsingGPS();

        changeContent(GoogleMapFragment.getInstance(), R.id.content, false);
        findViewById(R.id.menu_btn).setVisibility(View.VISIBLE);
    }

    private void stopGpsTrack(){
        if(((TestworkApp) getApplication()).getGpsPresenter()!=null)
            ((TestworkApp) getApplication()).getGpsPresenter().stopUsingGPS();
    }

    @Override
    public void onLocationChanged(BaseMarkerModel location) {
        if(places!=null)
            places.setPoint(location);

        data.getCompanyServices(location, String.valueOf(1000), this);
    }

    @Override
    public void call(List<BasePlaceModel> result) {
        if(places==null) {
            places = new PlacesListAdapter(result, ((TestworkApp) getApplication()).getGpsPresenter().getCurrentLocation());
            places.setOnItemClickListener(this);
            list.setAdapter(places);
        } else {
            places.setData(result);
        }

        Intent intent = new Intent(ACTION_SHOW_DATA);
        intent.putParcelableArrayListExtra(EXTRA_DATA, new ArrayList<>(result));
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void onItemClick(BasePlaceModel item) {
        Intent intent = new Intent(ACTION_TO_DATA);
        intent.putParcelableArrayListExtra(EXTRA_DATA, new ArrayList(){{add(item);}});
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        drawer.closeDrawers();
    }
}
