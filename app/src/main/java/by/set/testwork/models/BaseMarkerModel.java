package by.set.testwork.models;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Locale;

/**
 * Created by set.
 */

public class BaseMarkerModel implements Parcelable {

    private String id;
    private double latitude;
    private double longitude;

    public BaseMarkerModel(String id, double latitude, double longitude) {
        setId(id);
        setLatitude(latitude);
        setLongitude(longitude);
    }

    public BaseMarkerModel(double latitude, double longitude) {
        this.id = String.format(Locale.US, "%1f,%1f",latitude, longitude);
        setLatitude(latitude);
        setLongitude(longitude);
    }

    public BaseMarkerModel(Location loc) {
        this(loc.getLatitude(), loc.getLongitude());
    }

    public String getId() {
        return id;
    }

    public double getLatitude(){
        return latitude;
    }
    public double getLongitude(){
        return longitude;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String toQueryString(){
        return String.format(Locale.US, "%1f,%1f", getLatitude(), getLongitude());
    }

    public String toQueryYandexString(){
        return String.format(Locale.US, "%1f,%1f", getLongitude(),  getLatitude());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
        dest.writeDouble(getLatitude());
        dest.writeDouble(getLongitude());
    }

    public BaseMarkerModel(Parcel in){
        setId(in.readString());
        setLatitude(in.readDouble());
        setLongitude(in.readDouble());
    }

    public static final Parcelable.Creator<BaseMarkerModel> CREATOR = new Parcelable.Creator<BaseMarkerModel>() {
        public BaseMarkerModel createFromParcel(Parcel in) {
            return new BaseMarkerModel(in);
        }

        public BaseMarkerModel[] newArray(int size) {
            return new BaseMarkerModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public int hashCode(){
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof BaseMarkerModel && (this.getId().equals(((BaseMarkerModel) obj).getId()));
    }
}
