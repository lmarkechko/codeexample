package by.set.testwork.models.yandex;

import by.set.testwork.models.BaseMarkerModel;
import ru.yandex.yandexmapkit.utils.GeoPoint;

/**
 * Created by set.
 */
public class MarkerYandexModel extends BaseMarkerModel {

    public MarkerYandexModel(GeoPoint latln) {
       super(latln.getLat(), latln.getLon());
    }

    public GeoPoint getPosition() {
        return new GeoPoint(getLatitude(), getLongitude());
    }

}
