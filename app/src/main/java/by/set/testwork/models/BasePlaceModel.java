package by.set.testwork.models;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by set.
 */

public class BasePlaceModel implements Parcelable {

    private String id = "";
    private String name = "";
    private String icon = "";
    private String adres = "";
    private BaseMarkerModel point;

    public String getId(){
        return id;
    }
    public String getName(){
        return name;
    }
    public String getIcon() {
        return icon;
    }
    public String getAdres(){
        return adres;
    }
    public BaseMarkerModel getPoint(){
        return point;
    }

    public void setId(String id){
        this.id=id;
    }
    public void setName(String name){
        this.name=name;
    }
    public void setIcon(String icon){
        this.icon=icon;
    }
    public void setAdres(String adres){
        this.adres=adres;
    }
    public void setPoint(BaseMarkerModel point){
        this.point=point;
    }

    protected BasePlaceModel(){}

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
        dest.writeString(getName());
        dest.writeString(getIcon());
        dest.writeString(getAdres());
        getPoint().writeToParcel(dest, flags);
    }

    public BasePlaceModel(Parcel in){
        setId(in.readString());
        setName(in.readString());
        setIcon(in.readString());
        setAdres(in.readString());
        setPoint(new BaseMarkerModel(in));
    }

    public static final Parcelable.Creator<BasePlaceModel> CREATOR = new Parcelable.Creator<BasePlaceModel>() {
        public BasePlaceModel createFromParcel(Parcel in) {
            return new BasePlaceModel(in);
        }

        public BasePlaceModel[] newArray(int size) {
            return new BasePlaceModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof BasePlaceModel && (this.getId().equals(((BasePlaceModel) obj).getId()));
    }
}
