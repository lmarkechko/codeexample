package by.set.testwork.models.google;

import android.os.Parcel;

import by.set.testwork.models.BasePlaceModel;

/**
 * Created by set.
 */

public class ResultGoogle extends BasePlaceModel {

    private Geometry geometry;
    private String place_id = "";
    private String vicinity = "";

    public ResultGoogle(Parcel in) {
        super(in);
    }

    @Override
    public String getAdres() {
        return vicinity;
    }

    @Override
    public MarkerGoogleModel getPoint() {
        if(geometry!=null)
            return new MarkerGoogleModel(geometry.getLatLng());
        else
            return (MarkerGoogleModel) super.getPoint();
    }

    @Override
    public String getId() {
        return place_id;
    }

    @Override
    public void setId(String id) {
        super.setId(id);
        place_id=id;
    }

}
