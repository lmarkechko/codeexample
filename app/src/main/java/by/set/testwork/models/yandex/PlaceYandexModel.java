package by.set.testwork.models.yandex;

import by.set.testwork.models.BasePlaceModel;
import ru.yandex.yandexmapkit.utils.GeoPoint;

/**
 * Created by set.
 */

public class PlaceYandexModel extends BasePlaceModel {
    private GeoPoint latlng;

    public PlaceYandexModel(BasePlaceModel base){
        super();
        setId(base.getId());
        setName(base.getName());
        setAdres(base.getAdres());
        setPoint(base.getPoint());
    }

    public GeoPoint getPosition() {
        if(latlng==null)
            return latlng=new GeoPoint(getPoint().getLatitude(), getPoint().getLongitude());
        else
            return latlng;
    }
}
