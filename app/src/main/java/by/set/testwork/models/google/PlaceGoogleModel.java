package by.set.testwork.models.google;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import by.set.testwork.models.BasePlaceModel;

/**
 * Created by set.
 */

public class PlaceGoogleModel extends BasePlaceModel implements ClusterItem {
    private LatLng latlng;

    public PlaceGoogleModel(BasePlaceModel base){
        super();
        setId(base.getId());
        setName(base.getName());
        setAdres(base.getAdres());
        setPoint(base.getPoint());
    }

    @Override
    public LatLng getPosition() {
        if(latlng==null)
            return latlng=new LatLng(getPoint().getLatitude(), getPoint().getLongitude());
        else
            return latlng;
    }
}
