package by.set.testwork.models.google;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import by.set.testwork.models.BaseMarkerModel;

/**
 * Created by set.
 */
public class MarkerGoogleModel extends BaseMarkerModel implements ClusterItem {

    public MarkerGoogleModel(LatLng latln) {
       super(latln.latitude, latln.longitude);
    }

    @Override
    public LatLng getPosition() {
        return new LatLng(getLatitude(), getLongitude());
    }

}
