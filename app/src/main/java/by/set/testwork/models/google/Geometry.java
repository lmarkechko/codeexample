package by.set.testwork.models.google;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by set.
 */

public class Geometry {

    private LatLnLoc location;

    public LatLnLoc getLocation() {
        return location;
    }

    public LatLng getLatLng() {
        return location.getLatLng();
    }

    private class LatLnLoc {

        private double lat;
        private double lng;

        public double getLat() {
            return lat;
        }

        public double getLng() {
            return lng;
        }

        public LatLng getLatLng() {
            return new LatLng(lat, lng);
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }
}
