package by.set.testwork;

import android.os.Bundle;

import javax.inject.Inject;

import by.set.testwork.di.components.DaggerDataComponent;
import by.set.testwork.di.modules.DataModule;
import by.set.testwork.presenters.base.DataPresenter;

/**
 * Created by set.
 */

public class BaseDataActivity extends BaseFragmentActivity {

    @Inject
    DataPresenter data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerDataComponent.builder().dataModule(new DataModule(this)).build().inject(this);
    }

    public DataPresenter getData(){
        return data;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        data.unsubscribe();
    }
}
